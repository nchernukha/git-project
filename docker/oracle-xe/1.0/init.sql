CREATE TABLESPACE FTTABLESPACE
  DATAFILE 'FTTABLESPACE.dat'
  SIZE 10M AUTOEXTEND on;

CREATE TEMPORARY TABLESPACE FTTABLESPACE_TEMP
  TEMPFILE 'FTTABLESPACE_TEMP.dat'
  SIZE 5M AUTOEXTEND on;

CREATE USER FTPLATFORM
  IDENTIFIED BY FTPLATFORM
  DEFAULT TABLESPACE FTTABLESPACE
  TEMPORARY TABLESPACE FTTABLESPACE_TEMP
  QUOTA 20M on FTTABLESPACE;

GRANT create session TO FTPLATFORM;
GRANT create table TO FTPLATFORM;
GRANT create view TO FTPLATFORM;
GRANT create any trigger TO FTPLATFORM;
GRANT create any procedure TO FTPLATFORM;
GRANT create sequence TO FTPLATFORM;
GRANT create synonym TO FTPLATFORM;